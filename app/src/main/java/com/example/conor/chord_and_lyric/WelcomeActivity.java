package com.example.conor.chord_and_lyric;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class WelcomeActivity extends AppCompatActivity {
    private EditText editText;
    private Button button;
    private String Song;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        editText = (EditText) findViewById(R.id.Search);
        imageView = (ImageView) findViewById(R.id.Image);
        button = (Button) findViewById(R.id.Search_button);

        imageView.setImageResource(R.drawable.application_icon);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Song = editText.getText().toString();
                Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, Song);
                startActivity(intent);
            }
        });

    }
}
