package com.example.conor.chord_and_lyric.Utilites;

/**
 * Created by conor on 06/03/2017.
 */

public class Chord {
    private String chord;

    public Chord(String chord) {
        this.chord = chord;
    }

    public String getChord() {
        return chord;
    }

    @Override
    public String toString() {
        return "Chord{" +
                "chord='" + chord + '\'' +
                '}';
    }
}
