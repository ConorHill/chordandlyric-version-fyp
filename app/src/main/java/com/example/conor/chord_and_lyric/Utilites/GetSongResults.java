package com.example.conor.chord_and_lyric.Utilites;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.conor.chord_and_lyric.Utilites.Song.Parser;

/**
 * Created by conor on 23/02/2017.
 */

public class GetSongResults extends RawData {
    private static final String BASE_URL = "http://api.guitarparty.com/v2/songs/";
    private static final String API_KEY = "1dcc192f84b601659c8da7398778676be5ac0bb8";
    private static final String PARAM_QUERY = "query";
    private static final String Api_key = "api_key";
    private List<Song> Songs_list;
    private List<Artist> Artist_list;
    private List<Chord> Chord_list;
    private static Uri uri;


    public GetSongResults(String SearchCriteria) {
        super(SearchCriteria);
        BuildUrl(SearchCriteria);
        Songs_list = new ArrayList<Song>();
        Artist_list = new ArrayList<Artist>();
        Chord_list = new ArrayList<Chord>();
    }

    public void execute() {
        super.setUrl(uri.toString());
        GetResults results = new GetResults();
        results.execute(uri.toString());
    }

    public static URL BuildUrl(String Search) {
        uri = Uri.parse(BASE_URL).buildUpon()
                .appendQueryParameter(PARAM_QUERY, Search)
                .appendQueryParameter(Api_key, API_KEY)
                .build();
        URL url = null;
        try {
            url = new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    public void ProcessJsonSong() {
        final String SONG_TITLE = "title";
        final String SONG_OBJECT = "objects";
        final String SONG_BODY = "body";
        final String SONG_WEBLINK = "permalink";
        try {
            JSONObject object = new JSONObject(getData());
            JSONArray SongArray = object.getJSONArray(SONG_OBJECT);
            for (int i = 0; i < SongArray.length(); i++) {
                JSONObject jsonSong = SongArray.getJSONObject(i);
                String title = jsonSong.getString(SONG_TITLE);
                String song = jsonSong.getString(SONG_BODY);
                String weblink = jsonSong.getString(SONG_WEBLINK);
                Song SongObject = new Song(song, title, weblink);
                this.Songs_list.add(SongObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void ProcessJsonArtist() {
        final String SONG_ARTIST = "name";
        final String SONG_AUTHOR = "authors";
        final String SONG_OBJECT = "objects";

        try {
            JSONObject object = new JSONObject(getData());
            JSONArray songArray = object.getJSONArray(SONG_OBJECT);
            for (int i = 0; i < songArray.length(); i++) {
                JSONObject song = songArray.getJSONObject(i);
                JSONArray artist = song.getJSONArray(SONG_AUTHOR);
                for (int j = 0; j < artist.length(); j++) {
                    JSONObject art = artist.getJSONObject(j);
                    String Artist = art.getString(SONG_ARTIST);
                    Artist artist1 = new Artist(Artist);
                    this.Artist_list.add(artist1);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void ProcessJsonChords() {
        final String SONG_CHORDS = "chords";
        final String SONG_CHORD = "image_url";
        final String SONG_INSTRU = "instrument";
        final String SONG_TUNING = "tuning";
        final String SONG_OBJECT = "objects";

        try {
            JSONObject object = new JSONObject(getData());
            JSONArray songArray = object.getJSONArray(SONG_OBJECT);
            for (int i = 0; i < songArray.length(); i++) {
                JSONObject song = songArray.getJSONObject(i);
                JSONArray chords = song.getJSONArray(SONG_CHORDS);
                for (int j = 0; j < chords.length(); j++) {
                    JSONObject chord = chords.getJSONObject(j);
                    String[] Chords = new String[1000];
                    String url = chord.getString(SONG_CHORD);
                    Chord chord1 = new Chord(url);
                    Chord_list.add(chord1);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ;
        }
    }

    public List<Song> getSongs() {
        return Songs_list;
    }

    public List<Artist> getArtists() {
        return Artist_list;
    }

    public class GetResults extends DownloadData {
        @Override
        protected String doInBackground(String... urls) {
            String[] parm = {uri.toString()};
            return super.doInBackground(parm);
        }

        @Override
        protected void onPostExecute(String webData) {
            super.onPostExecute(webData);
            ProcessJsonSong();
            ProcessJsonArtist();
            ProcessJsonChords();
        }
    }
}
