package com.example.conor.chord_and_lyric.Utilites;

import java.io.Serializable;

/**
 * Created by conor on 02/03/2017.
 */

public class Artist implements Serializable{
    private String Artist;

    public Artist(String artist) {
        Artist = artist;
    }

    public String getArtist() {
        return Artist;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "Artist='" + Artist + '\'' +
                '}';
    }
}
