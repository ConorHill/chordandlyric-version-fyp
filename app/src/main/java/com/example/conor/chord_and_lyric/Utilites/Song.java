package com.example.conor.chord_and_lyric.Utilites;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by conor on 23/02/2017.
 */

public class Song implements Serializable {
    private String Song;
    private String Title;
    private String Weblink;

    public Song(String song, String title, String weblink) {
        Song = song;
        Title = title;
        Weblink = weblink;
    }

    public String getSong() {
        return Song;
    }

    public String getTitle() {
        return Title;
    }

    public String getWeblink() {
        return Weblink;
    }


    @Override
    public String toString() {
        return "Song{" +
                "Song='" + Song + '\'' +
                ", Title='" + Title + '\'' +
                ", Weblink='" + Weblink + '\'' +
                '}';
    }

    public static String Parser(String Song, int transpose) {
        Song=Song.replace("\r", "");
        Song=Song.replace("\n", "|");
        ArrayList<String> chords = new ArrayList<String>();
        ArrayList<String> lyrics = new ArrayList<String>();
        String cStack = "";
        String lStack = "";
        Boolean flag = false;

        for (int i = 0; i < Song.length(); i++) {

            char c = Song.charAt(i);

            if (c == '[') {
                flag = true;
            }
            if (c == ']') {
                flag = false;
            }
            if (flag && c != '[') {
                cStack += Song.charAt(i);
            } else if (!flag && c != '[' && c != ']' && c != '|') {
                cStack += " ";
                lStack += Song.charAt(i);
            }
            if (c == '|') {
                chords.add(cStack);
                lyrics.add(lStack);
                cStack = "";
                lStack = "";
            }
        }
        Song = "";
        if(transpose!=0) {
            for (int i = 0; i < chords.size(); i++) {

                String chordString = chords.get(i);
                char[] chordStringChars = chordString.toCharArray();

                for (int j = 0; j < chordStringChars.length; j++) {
                    if (chordStringChars[j] >= 'A' && chordStringChars[j] <= 'G') {
                        int letter = chordStringChars[j];
                        char transpose_character = (char) (letter + (transpose));
                        if(transpose_character=='H'){
                            transpose_character='A';
                        }
                        chordStringChars[j]=transpose_character;
                        if(transpose_character=='@'){
                            transpose_character='G';
                        }
                        chordStringChars[j]=transpose_character;
                    }
                }
                chordString = String.valueOf(chordStringChars);
                chords.set(i, chordString);
            }
        }
        for (int i = 0; i < chords.size(); i++) {
            Song += chords.get(i)+"\n";
            Song += lyrics.get(i) + "\n";
        }
        return Song;
    }
}
