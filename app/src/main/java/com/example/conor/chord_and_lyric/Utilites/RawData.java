package com.example.conor.chord_and_lyric.Utilites;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by conor on 23/02/2017.
 */

public class RawData {
    private String Url;
    private static String Data;

    public void setUrl(String url) {
        this.Url = url;
    }

    public static String getData() {
        return Data;
    }

    public RawData(String URL) {

        this.Url = URL;
    }

    public void execute() {
        DownloadData downloadData = new DownloadData();
        downloadData.execute(Url);
    }

    public class DownloadData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            String PageResults = null;
            try {
                URL Url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) Url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                InputStream IS = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(IS));
                StringBuffer buffer = new StringBuffer();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }
                return buffer.toString();
            } catch (IOException e) {
                e.printStackTrace();
                ;
            }
            return PageResults;
        }

        @Override
        protected void onPostExecute(String webData) {
            Data = webData;
        }
    }
}
