package com.example.conor.chord_and_lyric;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.conor.chord_and_lyric.Utilites.Artist;
import com.example.conor.chord_and_lyric.Utilites.Song;

import java.util.List;

/**
 * Created by conor on 27/02/2017.
 */

public class SongAdapter extends ArrayAdapter<Song>{
    private static List<Song> Songs;
    private static List<Artist> Artists;
    private int Resources;
    private Context context;

    public SongAdapter(Context context, int resource, List<Song> objects, List<Artist> writers) {
        super(context, resource, objects);
        Songs = objects;
        Artists = writers;
        Resources = resource;
        this.context = context;
    }

    @Override
    public int getCount() {
        if(Songs.size()==0){
            return 0;
        }
        else{
            return Songs.size();
        }
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RecordHolder holder;
        LayoutInflater inflater = ((Activity) context ).getLayoutInflater();

        if(convertView==null){
            convertView = inflater.inflate(R.layout.song_item, parent, false);
            holder = new RecordHolder();
            holder.SongName = (TextView) convertView.findViewById(R.id.Song_Item);
            convertView.setTag(holder);
        }
        else{
            holder = (RecordHolder) convertView.getTag();
        }
        Song songs = Songs.get(position);
        Artist artist = Artists.get(position);
        holder.SongName.setText(songs.getTitle()+"\n"+artist.getArtist());
        return convertView;
    }

    static class RecordHolder{
        TextView SongName;
    }
    public static Song getSong(int position){
        return(null != Songs ? Songs.get(position) : null);
    }
    public static Artist getArtist(int position){
        return (null != Artists ? Artists.get(position) : null);
    }
}
