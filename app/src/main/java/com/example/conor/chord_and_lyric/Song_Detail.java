package com.example.conor.chord_and_lyric;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.conor.chord_and_lyric.Utilites.Artist;
import com.example.conor.chord_and_lyric.Utilites.Song;

public class Song_Detail extends AppCompatActivity {
    private TextView Title;
    private TextView Body;
    private Button Scroll;
    private Button Audio;
    private Button Transpose;
    private ScrollView ScrollView;
    private Song song;
    private Artist artist;
    private String Song_body;
    private SeekBar seek;
    private int Time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.song__detail);
        Intent intent = getIntent();

        Title = (TextView) findViewById(R.id.Title);
        Body = (TextView) findViewById(R.id.Song_Body);
        Scroll = (Button) findViewById(R.id.Scroll_Button);
        ScrollView = (android.widget.ScrollView) findViewById(R.id.Scroll_View);
        seek = (SeekBar) findViewById(R.id.Seek);
        Audio = (Button) findViewById(R.id.audio);
        Transpose = (Button) findViewById(R.id.transpose);

        if (intent != null) {
            if (intent.hasExtra(Intent.EXTRA_TEXT)) {
                song = (Song) intent.getSerializableExtra(Intent.EXTRA_TEXT);
            }
            if(intent.hasExtra(Intent.EXTRA_PACKAGE_NAME)){
                artist = (Artist) intent.getSerializableExtra(Intent.EXTRA_PACKAGE_NAME);
            }
        }
        Song_body = Song.Parser(song.getSong(),0);
        Body.setMovementMethod(new ScrollingMovementMethod());
        Title.setText(song.getTitle()+"\n"+artist.getArtist());
        Body.setText(Song_body);

        SharedPreferences shared = getSharedPreferences("Mypref", MODE_PRIVATE);
        String query_main = shared.getString("Song_Name_Main_Search", null);
        String query_welcome = shared.getString("Song_Name_Welcome", null);
        Intent data = new Intent();
        if(query_welcome.length()>=0){
            data.setData(Uri.parse(query_welcome));
            setResult(RESULT_OK, data);
        }


        Scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Time = seek.getProgress()/2;
                ObjectAnimator.ofInt(ScrollView,"scrollY", ScrollView.getBottom()).setDuration(Time*1000).start();
            }
        });

        Audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Youtube = new Intent(Intent.ACTION_SEARCH);
                Youtube.setPackage("com.google.android.youtube");
                Youtube.putExtra("query", song.getTitle() + artist.getArtist());
                Youtube.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(Youtube);
            }
        });

        //Transpose Song Method
        Transpose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Create Alert Dialog
                AlertDialog.Builder alert = new AlertDialog.Builder(Song_Detail.this);
                alert.setTitle("Transpose");
                alert.setMessage("Enter Transpose");

                final EditText tran = new EditText(Song_Detail.this);
                LinearLayout.LayoutParams LP = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                tran.setLayoutParams(LP);
                alert.setView(tran);

                alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int transpose_by = Integer.parseInt(tran.getText().toString());
                        //Transpose song.
                        Body.setText(Song.Parser(song.getSong(), transpose_by));
                    }
                });
                alert.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.open_page, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int ID = item.getItemId();
        if(ID==R.id.weblink){
            GoToUrl(song.getWeblink());
        }
        return super.onOptionsItemSelected(item);
    }


    private void GoToUrl(String url){
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
