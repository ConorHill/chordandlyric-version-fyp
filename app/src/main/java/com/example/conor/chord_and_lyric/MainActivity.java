package com.example.conor.chord_and_lyric;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.conor.chord_and_lyric.Utilites.GetSongResults;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private SongAdapter songAdapter;
    private ProcessSongs processSongs;
    private String songName;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences shared = getApplicationContext().getSharedPreferences("pref",0);
        SharedPreferences.Editor edit = shared.edit();
        Intent intent = getIntent();
        if(intent!=null) {
            if (intent.hasExtra(Intent.EXTRA_TEXT)) {
                songName = intent.getStringExtra(Intent.EXTRA_TEXT);
            }
        }
//        edit.putString("SONG_NAME", songName);

        processSongs = new ProcessSongs(songName);
        listView = (ListView) findViewById(R.id.Song_List);
        progressBar = (ProgressBar) findViewById(R.id.Progress);
        if (IsNetworkAvailable()) {
            processSongs.execute();
        } else {
            Snackbar.make(findViewById(android.R.id.content), "You are not Connected", Snackbar.LENGTH_LONG)
                    .setAction("REFRESH", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            processSongs.execute();
                        }
                    }).show();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, Song_Detail.class);
                intent.putExtra(Intent.EXTRA_TEXT, SongAdapter.getSong(i));
                intent.putExtra(Intent.EXTRA_PACKAGE_NAME, SongAdapter.getArtist(i));
                startActivity(intent);
            }
        });

    }

    private boolean IsNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ProcessSongs processSongs = new ProcessSongs(query);
                processSongs.execute();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int ID = item.getItemId();
        if(ID==android.R.id.home){
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class ProcessSongs extends GetSongResults {
        public ProcessSongs(String SearchCriteria) {
            super(SearchCriteria);
        }

        @Override
        public void execute() {
            super.execute();
            ProcessSong processSong = new ProcessSong();
            processSong.execute();
        }

        public class ProcessSong extends GetResults {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);

            }

            @Override
            protected void onPostExecute(String webData) {
                super.onPostExecute(webData);
                progressBar.setVisibility(View.INVISIBLE);
                songAdapter = new SongAdapter(MainActivity.this, R.layout.song_item, getSongs(), getArtists());
                listView.setAdapter(songAdapter);
            }
        }
    }

}
